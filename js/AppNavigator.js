'use strict';
import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Drawer, Text, View} from "native-base";
import {StatusBar, Image, Platform, TouchableWithoutFeedback} from "react-native";
import {Actions, Router, Scene} from "react-native-router-flux";


import {statusBarColor} from "./themes/base-theme";
import {Ionicons} from "@expo/vector-icons";
import StartQuizPage from "./components/startQuizPage";
import Quiz from "./components/quiz";
import ReloadPage from "./components/reloadPage";


const RouterWithRedux = connect()(Router);


class AppNavigator extends Component {

  render() {  // eslint-disable-line class-methods-use-this
    return (
      <Drawer
        ref={(ref) => { this._drawer = ref; }}
        type="displace"
        tweenDuration={150}
        content={<View />}
        tapToClose
        acceptPan={true}
        onOpen={() => {}}
        onClose={() => {}}
        openDrawerOffset={0.2}
        panCloseMask={0.3}
        side="left"
        styles={{
          drawer: {
          },
        }}
        tweenHandler={(ratio) => {  //eslint-disable-line
          return {
            main: {
            },
          };
        }}
        negotiatePan
      >
        <StatusBar
            backgroundColor='transparent'
            barStyle="light-content"
        />
        <RouterWithRedux >
          <Scene key="root">
            <Scene key="startQuizPage" initial={true} component={StartQuizPage} />
            <Scene key="quiz" component={Quiz} />
            <Scene key="reloadPage" component={ReloadPage} />
          </Scene>
        </RouterWithRedux>
      </Drawer>
    );
  }
}

function bindAction(dispatch) {
  return {
  };
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps, bindAction)(AppNavigator);
