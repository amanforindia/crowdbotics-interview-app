'use strict';
import { createAction } from 'redux-actions';
import { login, apiCall } from '../util/api';
import { Actions, ActionConst } from 'react-native-router-flux';

export const getQuestions = function () {
    return async function(dispatch) {
      dispatch(isLoading(true));
      try {
        const data = await apiCall('https://opentdb.com/api.php?amount=10');
        console.log(data.results);
        dispatch(saveQuestions(data.results));
      } catch (err) {
        handleError(err);
      }
      dispatch(isLoading(false));
    };
};


const handleError = function(err) {
  return Promise.reject(err);
};

export const saveQuestions = createAction('SAVE_QUESTIONS');

export const saveScore = createAction('SAVE_SCORE');

export const saveTime = createAction('SAVE_TIME');

export const isLoading = createAction('IS_LOADING');