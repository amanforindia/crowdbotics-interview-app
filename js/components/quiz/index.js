/**
 * Created by aman on 19/12/17.
 */
'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform, Dimensions, AsyncStorage, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {Content, Text, Button, Icon, Left, Right, Body, Spinner, View, Input, ListItem, Radio} from 'native-base';
import Carousel from 'react-native-snap-carousel';
import styles from "./styles";
import commonColor from '../../../native-base-theme/variables/commonColor';
import {getQuestions, saveScore, saveTime} from "../../actions/actions";

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answers: props.questions.map(question => ''),
      initialTime: new Date().getTime()
    }
  }

  allAnswersSelected(answers) {
    for (let i = 0; i < answers.length; i++) {
      if (answers[i] === '') {
        return false;
      }
    }
    return true;
  }

  getScore(questions, answers) {
    let score = 0;
    for (let i = 0; i < questions.length; i++) {
      if (answers[i] === questions[i].correct_answer) {
        score++;
      }
    }
    return score;
  }

  componentDidUpdate() {
    if (this.allAnswersSelected(this.state.answers)) {
      this.props.saveScore(this.getScore(this.props.questions, this.state.answers));
      this.props.saveTime((new Date().getTime()) - this.state.initialTime);
      Actions.reloadPage();
    }
  }

  render() {
    return (
      <View style={[styles.container, {flexDirection: 'column'}]}>
        <Content contentContainerStyle={{flex: 0, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center'}}>
          <View style={{margin: 20,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',}}>
            {this.props.questions ? this.props.questions.map((question, idx) => {
              let options = question.incorrect_answers.map(a => a);
              options.push(question.correct_answer);
              //options = shuffle(options);
              return <View>
              <Text>{question.question}</Text>
                {options.map(option => <ListItem style={{}}>
                <Text style={{backgroundColor: 'white'}}>{option}</Text>
                <View style={{backgroundColor: 'white', paddingLeft: 20, paddingRight: 20}}>
                  <Radio selected={this.state.answers[idx] === option} onPress={() => {
                    let answers = this.state.answers;
                    answers[idx] = option;
                    this.setState({answers});
                  }} />
                </View>
              </ListItem>)}
            </View>; }) : null}
          </View>
        </Content>
          {this.props.isLoading ? <View spinner>
            <Spinner color={commonColor.defaultSpinnerColor} />
          </View> : null}
      </View>
    );
  }
}


function bindActions(dispatch){
  return {
    saveScore: score => dispatch(saveScore(score)),
    saveTime: score => dispatch(saveTime(score))
  };
}
const mapStateToProps = state => ({
    isLoading: state.reducers.isLoading,
    questions: state.reducers.questions
});


export default connect(mapStateToProps, bindActions)(Quiz);
