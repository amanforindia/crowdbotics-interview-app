
'use strict';

import { StyleSheet } from "react-native";
var React = require('react-native');
var { Dimensions } = React;
import colors from '../../../native-base-theme/variables/commonColor';

var deviceHeight = Dimensions.get('window').height;
var deviceWidth = Dimensions.get('window').width;

const horizontalMargin = 20;
const slideWidth = 280;

const sliderWidth = Dimensions.get('window').width;
const itemWidth = horizontalMargin * 7;
const itemHeight = 200;


export default {
  iosLogoContainer: {
    top: 40,
    alignItems: 'center',
    width: '100%',
    position: 'absolute'
  },
  aLogoContainer: {
    top: 40,
    alignItems: 'center',
    height: deviceHeight/1.5,
    position: 'absolute',
    width: '100%'
  },
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: '#000'
  },
  content: {
    flex: 1,
    justifyContent: 'center'
  },
  logoIcon: {
    color: '#eee',
    fontSize: 100
  },
  logoText: {
    color: '#eee',
    fontWeight: '700',
    fontSize: 25,
    lineHeight: 30,
    marginTop: -10
  },
  loginBtn: {
    borderRadius: 25,
    width: 250,
    backgroundColor: colors.btnPrimaryBg,
    alignItems: 'center',
    justifyContent: 'center'
  },
  registerBtn: {
    borderRadius: 25,
    elevation: 0,
    borderColor: colors.btnPrimaryBg,
    width: 250,
    backgroundColor: colors.btnPrimaryColor,
    alignItems: 'center',
    justifyContent: 'center'
  },slide: {
        width: itemWidth,
        height: itemHeight,
        paddingHorizontal: horizontalMargin
        // other styles for the item container
    },
    slideInnerContainer: {
        width: slideWidth,
        flex: 1
        // other styles for the inner container
    }
};
