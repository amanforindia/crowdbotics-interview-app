/**
 * Created by aman on 19/12/17.
 */
'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Platform, Dimensions, AsyncStorage, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {Content, Text, Button, Icon, Left, Right, Body, Spinner, View, Input} from 'native-base';
import Carousel from 'react-native-snap-carousel';
import styles from "./styles";
import commonColor from '../../../native-base-theme/variables/commonColor';
import {getQuestions} from "../../actions/actions";

class ReloadPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: ''
    }
  }

  async getQuestions() {
    await this.props.getQuestions();
    Actions.quiz();
  }

  render() {
    return (
      <View style={[styles.container, {paddingHorizontal: 50}]}>
        <Content contentContainerStyle={styles.content}>
          <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
            <Text>Score: {this.props.score}/10</Text>
            <Text>Time: {this.props.time/(1000*60)} minutes</Text>
            <Button onPress={() => this.getQuestions()} rounded style={{alignSelf: 'center'}}>
              <Text style={{textAlign: 'center'}}>Play again</Text>
            </Button>
          </View>
        </Content>
          {this.props.isLoading ? <View spinner>
            <Spinner color={commonColor.defaultSpinnerColor} />
          </View> : null}
      </View>
    );
  }
}


function bindActions(dispatch){
  return {
    getQuestions: (code) => dispatch(getQuestions(code))
  };
}
const mapStateToProps = state => ({
    isLoading: state.reducers.isLoading,
  score: state.reducers.score,
  time: state.reducers.time
});


export default connect(mapStateToProps, bindActions)(ReloadPage);
