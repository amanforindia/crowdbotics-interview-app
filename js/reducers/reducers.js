/**
 * Created by aman on 18/6/17.
 */

import {handleActions} from 'redux-actions';
import * as actions from '../actions/actions';

const initialState = {
  questions: null,
  isLoading: 0,
  score: 0,
  time: 0
};

export const reducers = handleActions({
    [actions.saveQuestions]: (state, action) => ({
      ...state,
      questions: action.payload
    }),
  [actions.isLoading]: (state, action) => ({
    ...state,
    isLoading: action.payload ? (state.isLoading + 1) : Math.max(state.isLoading - 1, 0)
    //isLoading: state.isLoading
  }),
  [actions.saveScore]: (state, action) => ({
    ...state,
    score: action.payload
  }),
  [actions.saveTime]: (state, action) => ({
    ...state,
    time: action.payload
  }),
}, initialState);
