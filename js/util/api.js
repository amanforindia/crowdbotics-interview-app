/**
 * Created by aman on 18/6/17.
 */
'use strict';

//import { BASE_API_URL } from '../constants';
import {config} from '../../app.json';
import {AsyncStorage} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';

export const apiCall = function (url, type, body) {
  let params = {
    method: type || 'GET',
    headers: {'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    }
  };

  if (type === 'POST' || type === 'PATCH' || type === 'PUT') {
    params['body'] = JSON.stringify(body);
    console.log(params['body']);
  }
  return fetch(url, params)
    .then(response => {
        return response.text();
    }, err => Promise.reject(err))
    .then(json => {
        console.log(json);
      if (json === '') return Promise.resolve(null);
      json = JSON.parse(json);
      return Promise.resolve(json);
    }, err => Promise.reject(err));
};