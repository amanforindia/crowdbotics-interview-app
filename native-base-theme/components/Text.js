import { Platform } from 'react-native';
import _ from 'lodash';

import variable from './../variables/platform';

export default (variables = variable) => {
  const textTheme = {
      fontSize: variables.DefaultFontSize - 1,
      fontFamily: variables.fontFamily,
      color: variables.textColor,
    textAlign: 'center',
      '.note': {
        color: variables.lightThemePlaceholder,
        fontSize: variables.noteFontSize
      },
      '.header': {
        fontSize: variables.titleFontSize,
        fontWeight: '500'
      },
      '.headerxl': {
        fontSize: 20,
        fontWeight: '600'
      }
  };

  return textTheme;
};
