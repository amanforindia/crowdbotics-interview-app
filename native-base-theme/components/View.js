import { Platform } from 'react-native';
import _ from 'lodash';

import variable from './../variables/platform';

export default (variables = variable) => {
  const viewTheme = {
      '.padder': {
        padding: variables.contentPadding,
      },
      '.spinner': {
          position: 'absolute', flex: 1, left: 0,
          backgroundColor: 'white', right: 0, top: 0,
          bottom: 0, opacity: 0.8, justifyContent: 'center', alignItems: 'center'
      }
  };


  return viewTheme;
};
